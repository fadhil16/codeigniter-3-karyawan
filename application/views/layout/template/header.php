<!DOCTYPE html>
<html>
    <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>DNP Indonesia</title>
      <!-- Menghubungkan file CSS Bootstrap -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
      <style>
          /* Menambahkan sedikit gaya pada tabel */
          .table {
            margin-top: 20px;
            padding: 20px;
            background-color: #f7f7f7;
          }
          .table-wrapper h2 {
            margin-bottom: 20px;
          }
          .action-buttons {
            text-align: center;
          }
          .action-buttons a {
            margin-right: 10px;
          }
          .button-group {
            display: flex;
            justify-content: space-between;
          }
          .form-wrapper {
            margin: 20px;
            padding: 20px;
            background-color: #f7f7f7;
          }
  
          .form-wrapper h2 {
            margin-bottom: 20px;
          }
      
          .form-group {
            margin-bottom: 20px;
          }
      
          .form-label {
            font-weight: bold;
          }
      
          .form-control {
            width: 100%;
          }
      
          .btn-submit {
            margin-top: 20px;
          }
          
          .container-home {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            flex-direction: column;
          }
          
          .navbar-nav.ml-auto.justify-content-end {
            display: flex;
            justify-content: flex-end;
            align-items: center;
          }
        
          .navbar-nav.ml-auto.justify-content-end .nav-link {
            margin-right: 10px;
          }
        </style>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <!-- <div class="container"> -->
                    <a class="navbar-brand" href="#">DNP Indonesia</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                      <ul class="navbar-nav ml-auto justify-content-end">
                        <li class="nav-item">
                          <a class="nav-link" href="<?php echo site_url('home'); ?>">Home</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="<?php echo site_url('/'); ?>">Karyawan</a>
                        </li>
                        <li class="nav-item">
                          <?php if ($this->session->userdata('logged_in')) { ?>
                              <a class="nav-link" href="<?php echo site_url('logout'); ?>" >Logout</a>
                          <?php } else { ?>
                              <a class="nav-link" href="<?php echo site_url('login'); ?>" >Login</a>
                          <?php } ?>
                        </li>
                      </ul>
                    </div>
                <!-- </div> -->
            </nav>
        </header>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
