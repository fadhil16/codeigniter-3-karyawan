<?php include 'layout/template/header.php'; ?>
    <div class="container">
        <div class="table-wrapper">
            <h2>Daftar Karyawan</h2>
            <a href="<?php echo site_url('create'); ?>" class="btn btn-primary">Tambah</a>
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>Nama</th>
                        <th>NIK</th>
                        <th>Alamat</th>
                        <th>Telepon</th>
                        <th>Jabatan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($karyawan as $row) { ?>
                        <tr>
                            <td><?php echo $row->nama; ?></td>
                            <td><?php echo $row->nik; ?></td>
                            <td><?php echo $row->alamat; ?></td>
                            <td><?php echo $row->telp; ?></td>
                            <td><?php echo $row->jabatan; ?></td>
                            <td class="action-buttons">
                                <a href="<?php echo site_url('edit/' . $row->id); ?>" class="btn btn-primary">Edit</a>
                                <a href="<?php echo site_url('delete/' . $row->id); ?>" class="btn btn-danger">Delete</a>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
