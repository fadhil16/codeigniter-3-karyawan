<?php include 'layout/template/header.php'; ?>
    <div class="container">
        <div class="form-wrapper">
            <h2>Edit Data Karyawan</h2>
            <?php if (isset($validation_errors) && !empty($validation_errors)): ?>
                <div class="alert alert-danger">
                    <?php echo $validation_errors; ?>
                </div>
            <?php endif; ?>
            <form action="<?php echo site_url('update/' . $karyawan->id); ?>" method="post">
                <div class="form-group">
                    <label for="nama" class="form-label">Nama</label>
                    <input type="text" id="nama" name="nama" class="form-control" value="<?php echo $karyawan->nama; ?>">
                </div>
                <div class="form-group">
                    <label for="nik" class="form-label">NIK</label>
                    <input type="text" id="nik" name="nik" class="form-control" value="<?php echo $karyawan->nik; ?>">
                </div>
                <div class="form-group">
                    <label for="alamat" class="form-label">Alamat</label>
                    <textarea id="alamat" name="alamat" class="form-control"><?php echo $karyawan->alamat; ?></textarea>
                </div>
                <div class="form-group">
                    <label for="telp" class="form-label">Telepon</label>
                    <input type="text" id="telp" name="telp" class="form-control" value="<?php echo $karyawan->telp; ?>">
                </div>
                <div class="form-group">
                    <label for="jabatan" class="form-label">Jabatan</label>
                    <select id="jabatan" name="jabatan" class="form-control">
                        <option value="1" <?php echo ($karyawan->jabatan == 1) ? 'selected' : ''; ?>>Manager</option>
                        <option value="2" <?php echo ($karyawan->jabatan == 2) ? 'selected' : ''; ?>>Staff</option>
                        <option value="3" <?php echo ($karyawan->jabatan == 3) ? 'selected' : ''; ?>>Supervisor</option>
                        <option value="4" <?php echo ($karyawan->jabatan == 4) ? 'selected' : ''; ?>>Karyawan</option>
                    </select>
                </div>
            </form>
            <div class="button-group d-flex justify-content-between">
                <a href="<?php echo site_url('/'); ?>" class="btn btn-danger">Kembali</a>
                <button type="submit" class="btn btn-primary btn-submit">Simpan</button>
            </div>
        </div>
    </div>

