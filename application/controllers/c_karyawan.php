<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_karyawan extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_karyawan');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    public function home()
    {
        $this->load->view('home');
    }

    public function login()
    {
        $this->load->view('login');
    }

    public function login_required()
    {
        if (!$this->session->userdata('logged_in')) {
            $this->session->set_userdata('redirect_page', $this->uri->uri_string());

            //Jika belum login, redirect ke halaman login
            $id = $this->uri->segment(3);
            if (!empty($id)) {
                $this->session->set_userdata('redirect_page', $id);
            }
            
            redirect('login');
        } else {
            return TRUE;
        }
    }

    public function auth()
    {
        // Validasi Input
        $this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
        $this->form_validation->set_rules('password', 'Password', 'required|alpha_numeric');
    
        // Verifikasi Login
        if ($this->form_validation->run() == FALSE) {
            // Validasi gagal, kembali ke halaman login dengan pesan error
            $data['error_message'] = "Username atau Password salah";
            $this->load->view('login', $data);
        } else {
            // Validasi berhasil, lakukan verifikasi username dan password
            $result = $this->m_karyawan->auth();
            if ($result) {
                // Username dan password benar
                $session_data = array(
                    'id' => $result->id,
                    'username' => $result->username,
                    'logged_in' => TRUE
                );
                $this->session->set_userdata($session_data);

                //Cek halaman yang ingin diakses sebelumnya
                $redirect_page = $this->session->userdata('redirect_page');
                $this->session->unset_userdata('redirect_page');

                if ($redirect_page == 'create') {
                    redirect('create');
                } elseif ($redirect_page == 'edit') {
                    redirect('edit' . $this->session->userdata('redirect_id'));
                } elseif ($redirect_page == 'delete') {
                    redirect('delete' . $this->session->userdata('redirect_id'));
                } else {
                    redirect('home');
                }
            }
        }
    }
    
    public function logout()
    {
        $this->session->unset_userdata('logged_in');
        redirect('login');
    }    

    public function index()
    {
        $data['karyawan'] = $this->m_karyawan->get_all();
        $this->load->view('index', $data);
    }

    public function create()
    {
        // Cek apakah pengguna sudah login
        $this->login_required();        
        $this->load->view('create');
    }

    public function store()
    {
        // Validasi input
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('nik', 'NIK', 'required|numeric|min_length[10]|max_length[16]');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('telp', 'Telepon', 'required|numeric');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
    
        if ($this->form_validation->run() == FALSE) {
            // Validasi gagal, kembali ke form create karyawan dengan pesan error
            $data['validation_errors'] = validation_errors();
            $this->load->view('create', $data);
        } else {
            // Validasi berhasil, lakukan pemrosesan data karyawan baru
            $data = array(
                'nama' => $this->input->post('nama'),
                'nik' => $this->input->post('nik'),
                'alamat' => $this->input->post('alamat'),
                'telp' => $this->input->post('telp'),
                'id_jabatan' => $this->input->post('jabatan'),
            );
    
            $this->m_karyawan->store($data);
            redirect('/');
        }
    } 

    public function edit($id)
    {
        // Cek apakah pengguna sudah login
        $this->login_required();
        $data['karyawan'] = $this->m_karyawan->get_by_id($id);
        $this->load->view('edit', $data);
    }

    public function update($id) 
    {
        // Cek apakah pengguna sudah login
        if (!$this->session->userdata('logged_in')) {
            redirect('login'); // Redirect pengguna ke halaman login jika belum login
        }

        // Validasi input
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('nik', 'NIK', 'required|numeric|max_length[16]');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('telp', 'Telepon', 'required|numeric');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');

        if ($this->form_validation->run() == FALSE) {
            // Validasi gagal, kembali ke form edit karyawan dengan pesan error
            $data['karyawan'] = $this->m_karyawan->get_by_id($id);
            $data['validation_errors'] = validation_errors(); // Capture validation errors
            $this->load->view('edit', $data);
        } else {
            // Validasi berhasil, lakukan pemrosesan data update karyawan
            $data = array(
                'nama' => $this->input->post('nama'),
                'nik' => $this->input->post('nik'),
                'alamat' => $this->input->post('alamat'),
                'telp' => $this->input->post('telp'),
                'id_jabatan' => $this->input->post('jabatan'),
            );

            $this->m_karyawan->update($id, $data);
            redirect('/');
        }
    }


    public function delete($id)
    {
        $this->m_karyawan->delete($id);
        redirect('/');
    }
}