<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_karyawan extends CI_Model {
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function auth()
    {
        $this->db->where('username', $this->input->post('username'));
        $this->db->where('password', md5($this->input->post('password')));
        $query = $this->db->get('users');
        return $query->row();
    }
    
    public function get_all()
    {
        $this->db->select('karyawan.*, jabatan.nama as jabatan');
        $this->db->from('karyawan');
        $this->db->join('jabatan', 'jabatan.id = karyawan.id_jabatan', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_by_id($id)
    {
        $this->db->select('karyawan.*, jabatan.nama as jabatan');
        $this->db->from('karyawan');
        $this->db->join('jabatan', 'jabatan.id = karyawan.id_jabatan', 'left');
        $this->db->where('karyawan.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function store($data)
    {
        $this->db->insert('karyawan', $data);
        return $this->db->insert_id();
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('karyawan', $data);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('karyawan');
    }
}